<?php

declare(strict_types=1);

namespace App\Repositories\Contracts
{
    interface CustomerRepository
    {
		/**
         * @param  string $id
         * @param  array  $columns
         * @param  array  $relations
         * @return array
         * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
         */
		public function findById(string $ownerId, string $id, array $columns = ['*'], array $relations = []): array;

		/**
         * @param  string $id
         * @param  array  $attributes
         * @return array
         * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
         */
		public function update(string $ownerId, string $id, array $attributes): array;

        /**
         * @param  array  $attributes
         * @return array
         */
        public function create(string $ownerId, array $attributes): array;

        /**
         * @param  string $id
         * @return bool
         */
        public function delete(string $ownerId, string $id): bool;

        /**
         * @param  array  $columns
         * @param  array  $relations
         * @return array
         */
        public function all(string $ownerId, array $columns = ['*'], array $relations = []): array;
    }
}
