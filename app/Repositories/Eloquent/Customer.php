<?php

declare(strict_types=1);

namespace App\Repositories\Eloquent
{
    use Illuminate\Support\Collection;
    use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
	use App\Repositories\Contracts\CustomerRepository;
    use App\Models\Customer as CustomerModel;

	class Customer implements CustomerRepository
	{
		/**
		 * @param  string $id
         * @param  array  $columns
		 * @param  array  $relations
         * @return array
         * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
		 */
		public function findById(string $ownerId, string $id, array $columns = ['*'], array $relations = []): array
        {
            if (empty($columns)) {
                $columns = ['*'];
            }

            if (!empty($relations)) {
                $result = CustomerModel::with($relations)->where('user_id', $ownerId)->find($id, $columns);
            } else {
                $result = CustomerModel::where('user_id', $ownerId)->find($id, $columns);
            }

            if (isset($result)) {
                return $result->toArray();
            }

            throw new NotFoundHttpException("No customer record found with a id of '{$id}'");
        }

		/**
		 * @param  string $id
		 * @param  array  $attributes
         * @return array
         * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
		 */
		public function update(string $ownerId, string $id, array $attributes): array
        {
            $result = CustomerModel::where('user_id', $ownerId)->find($id);
            $attributes = Collection::make($attributes);

            if (isset($result)) {
                $result->update($attributes->only(['company_name', 'first_name', 'last_name', 'phone_number'])->toArray());
                $result->address->update($attributes->only(['address_1', 'address_2', 'address_3', 'city', 'country', 'post_code', 'lat', 'long'])->toArray());

                return $result->toArray();
            }

            throw new NotFoundHttpException("No customer record found with a id of '{$id}'");
        }

        /**
         * @param  array  $attributes
         * @return array
         */
        public function create(string $ownerId, array $attributes): array
        {
            $attributes['user_id'] = $ownerId;
            $attributes = Collection::make($attributes);

            $customer = CustomerModel::create($attributes->only(['user_id', 'company_name', 'first_name', 'last_name', 'phone_number'])->toArray());
            $customer->address()->create($attributes->only(['address_1', 'address_2', 'address_3', 'city', 'country', 'post_code', 'lat', 'long'])->toArray());

            return $customer->toArray();
        }

        /**
         * @param  string $id
         * @return bool
         */
        public function delete(string $ownerId, string $id): bool
        {
            $result = CustomerModel::where('user_id', $ownerId)->find($id);

            if (isset($result)) {
                return $result->delete();
            }

            throw new NotFoundHttpException("No customer record found with a id of '{$id}'");
        }

        /**
         * @param  array  $columns
         * @param  array  $relations
         * @return array
         */
        public function all(string $ownerId, array $columns = ['*'], array $relations = []): array
        {
            if (empty($columns)) {
                $columns = ['*'];
            }

            if (!empty($relations)) {
                $result = CustomerModel::with($relations)->where('user_id', $ownerId)->get($columns);
            } else {
                $result = CustomerModel::where('user_id', $ownerId)->get($columns);
            }

            return $result->toArray();
        }
	}
}
