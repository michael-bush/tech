<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RecoverAccount extends Mailable
{
    use Queueable, SerializesModels;

    protected $recoveryCode;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(string $recoveryCode)
    {
        $this->recoveryCode = $recoveryCode;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.auth.recover-account', [
            'recoveryCode' => $this->recoveryCode,
        ]);
    }
}
