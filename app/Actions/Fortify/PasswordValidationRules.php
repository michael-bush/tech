<?php

namespace App\Actions\Fortify;

use Laravel\Fortify\Rules\Password;
use App\Rules\SafePassword;

trait PasswordValidationRules
{
    /**
     * Get the validation rules used to validate passwords.
     *
     * @return array
     */
    protected function passwordRules()
    {
        $password = new Password;
        $password->requireUppercase();
        $password->requireNumeric();

        return ['required', 'string', $password, 'confirmed', new SafePassword];
    }
}
