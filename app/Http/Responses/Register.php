<?php

declare(strict_types=1);

namespace App\Http\Responses
{
    use Illuminate\Contracts\Auth\StatefulGuard;
    use Laravel\Fortify\Http\Responses\RegisterResponse as FortifyRegisterResponse;

    class Register extends FortifyRegisterResponse
    {
        protected $guard;

        public function __construct(StatefulGuard $guard)
        {
            $this->guard = $guard;
        }

        public function toResponse($request)
        {
            $this->guard->logout();

            return $request->wantsJson()
                ? new JsonResponse('', 201)
                : redirect(route('login'))->with('status', 'You have successfully created an account, please check your email for the verification link.');
        }
    }
}
