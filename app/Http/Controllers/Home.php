<?php

declare(strict_types=1);

namespace App\Http\Controllers
{
    use Illuminate\Http\Request;

    class Home extends Controller
    {
        public function welcome()
        {
            return view('welcome');
        }

        /**
         * Show the application dashboard.
         *
         * @return \Illuminate\Contracts\Support\Renderable
         */
        public function index()
        {
            $recoveryCodes = [];

            if (auth()->user()->two_factor_secret) {
                $recoveryCodes = json_decode(decrypt(auth()->user()->two_factor_recovery_codes));
            }

            return view('home', compact('recoveryCodes'));
        }
    }
}
