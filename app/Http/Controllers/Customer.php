<?php

declare(strict_types=1);

namespace App\Http\Controllers
{
    use Illuminate\Http\Request;
    use Yajra\Datatables\Datatables;
    use App\Services\Contracts\CustomerService;

    class Customer extends Controller
    {
        private $customerService;

        public function __construct(CustomerService $service)
        {
            $this->customerService = $service;
        }

        /**
         * Display a listing of the resource.
         */
        public function index(Request $request)
        {
            return view('customers.index');
        }

        /**
         * Display a listing of the resource.
         */
        public function api(Request $request)
        {
            $customers = $this->customerService->fetchAll($request->user()->id);
    
            return Datatables::of($customers)
                ->addColumn('action', function ($customer) {
                    return '<a href="' . route('customers.show', $customer['id']) . '" class="btn btn-xs btn-primary">Manage</a>';
                })
                ->make(true);
        }
    
        /**
         * Display a listing of the resource.
         */
        public function show(Request $request, string $id)
        {
            $customer = $this->customerService->fetch($request->user()->id, $id);
            return view('customers.show', compact('customer'));
        }

        /**
         * Display a listing of the resource.
         */
        public function create(Request $request)
        {
            return view('customers.create');
        }

        /**
         * Update the specified resource in storage.
         */
        public function store(Request $request)
        {
            $this->customerService->create(
                $request->user()->id,
                $request->only([
                    'company_name',
                    'first_name',
                    'last_name',
                    'phone_number',
                    'address_1',
                    'address_2',
                    'address_3',
                    'city',
                    'post_code',
                    'country'
                ])
            );

            return redirect()->route('customers')->withStatus('Customer data has been saved');
        }
    
        /**
         * Show the form for editing the specified resource.
         */
        public function edit(Request $request, string $id)
        {
            $customer = $this->customerService->fetch($request->user()->id, $id);
            return view('customers.edit', compact('customer'));
        }
    
        /**
         * Update the specified resource in storage.
         */
        public function update(Request $request, string $id)
        {
            $this->customerService->update(
                $request->user()->id,
                $id,
                $request->only([
                    'company_name',
                    'first_name',
                    'last_name',
                    'phone_number',
                    'address_1',
                    'address_2',
                    'address_3',
                    'city',
                    'post_code',
                    'country'
                ])
            );

            return redirect()->route('customers.show', $id)->withStatus('Customer data has been updated');
        }
    
        /**
         * Remove the specified resource from storage.
         */
        public function destroy(Request $request, string $id)
        {
            $this->customerService->delete($request->user()->id, $id);
            return redirect()->route('customers')->withStatus('Customer has been deleted');
        }
    }
}
