<?php

declare(strict_types=1);

namespace App\Http\Controllers
{
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Mail;
    use App\Models\User;
    use App\Mail\RecoverAccount;

    class Auth extends Controller
    {
        public function verify()
        {
            return view('auth.verify');
        }

        public function resend(Request $request)
        {
            $request->user()->sendEmailVerificationNotification();
            return back()->with('message', 'Verification link sent!');
        }

        public function sendRecoveryCode(Request $request)
        {
            if (! $request->session()->has('login.id') ||
                ! $user = User::find($request->session()->pull('login.id'))) {
                return redirect()->route('login')->with('status', 'There was an error sending you your recovery code.');
            }

            if ($user->two_factor_secret) {
                $recoveryCodes = json_decode(decrypt($user->two_factor_recovery_codes));
                Mail::to($user)->send(new RecoverAccount($recoveryCodes[0]));

                return redirect()->route('login')->with('status', 'Recovery code has been sent.');
            } else {
                return redirect()->route('login')->with('status', '2-Step Verification is disabled.');
            }
        }
    }
}
