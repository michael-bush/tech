<?php

declare(strict_types=1);

namespace App\Providers
{
    use Illuminate\Support\ServiceProvider;

    class ServicesServiceProvider extends ServiceProvider
    {
        /**
         * Register any application services.
         *
         * @return void
         */
        public function register()
        {
            $this->app->bind(
                \App\Services\Contracts\CustomerService::class,
                \App\Services\Customer::class
            );
        }

        /**
         * Bootstrap any application services.
         *
         * @return void
         */
        public function boot()
        {
        }
    }
}
