<?php

declare(strict_types=1);

namespace App\Providers
{
    use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
    use Illuminate\Support\Facades\Gate;
    use App\Models\User;
    use App\Models\Permission;
    use Illuminate\Support\Facades\Cache;

    class AuthServiceProvider extends ServiceProvider
    {
        /**
         * The policy mappings for the application.
         *
         * @var array
         */
        protected $policies = [
            // 'App\Models\Model' => 'App\Policies\ModelPolicy',
        ];

        /**
         * Register any authentication / authorization services.
         *
         * @return void
         */
        public function boot()
        {
            $this->registerPolicies();

            $cacheKey = 'permissions';
            $permissions = Cache::get($cacheKey);

            if (empty($permissions)) {
                $permissions = Permission::pluck('name');
                Cache::put($cacheKey, $permissions->toArray());
            } else {
                $permissions = collect($permissions);
            }

            $permissions->each(function(string $name) {
                Gate::define($name, function (User $user) use($name) {
                    return ($user->role->permissions()->where('name', $name)->count() > 0);
                });
            });
        }
    }
}
