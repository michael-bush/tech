<?php

declare(strict_types=1);

namespace App\Services\Contracts
{
    use Illuminate\Support\Collection;

    interface CustomerService
    {
        /**
         * @param  string $ownerId
         *
         * @return \Illuminate\Support\Collection
         */
        public function fetchAll(string $ownerId): Collection;

        /**
         * @param  string $ownerId
         * @param  string $userId
         *
         * @return \Illuminate\Support\Collection
         */
        public function fetch(string $ownerId, string $userId): Collection;

        /**
         * @param  string $ownerId
         * @param  string $userId
         *
         * @return bool
         */
        public function delete(string $ownerId, string $userId): bool;

        /**
         * @param  string $ownerId
         * @param  string $id
         * @param  array  $fields
         *
         * @return \Illuminate\Support\Collection
         */
        public function update(string $ownerId, string $id, array $fields): Collection;

        /**
         * @param  string $ownerId
         * @param  array  $fields
         *
         * @return \Illuminate\Support\Collection
         */
        public function create(string $ownerId, array $fields): Collection;
    }
}
