<?php

declare(strict_types=1);

namespace App\Services
{
    use App\Services\Contracts\CustomerService;
    use App\Repositories\Contracts\CustomerRepository;
    use Illuminate\Support\Collection;

    class Customer implements CustomerService
    {
        private $customerRepository;
    
        public function __construct(CustomerRepository $repository)
        {
            $this->customerRepository = $repository;
        }

        /**
         * @param  string $ownerId
         *
         * @return \Illuminate\Support\Collection
         */
        public function fetchAll(string $ownerId): Collection
        {
            return Collection::make($this->customerRepository->all($ownerId));
        }

        /**
         * @param  string $ownerId
         * @param  string $userId
         *
         * @return \Illuminate\Support\Collection
         */
        public function fetch(string $ownerId, string $id): Collection
        {
            return Collection::make($this->customerRepository->findById($ownerId, $id, ['*'], ['address']));
        }

        /**
         * @param  string $ownerId
         * @param  string $userId
         *
         * @return bool
         */
        public function delete(string $ownerId, string $id): bool
        {
            return $this->customerRepository->delete($ownerId, $id);
        }

        /**
         * @param  string $ownerId
         * @param  array  $fields
         *
         * @return \Illuminate\Support\Collection
         */
        public function update(string $ownerId, string $id, array $fields): Collection
        {
            $data = Collection::make($fields);
            $data = $data->only([
                'address_1',
                'address_2',
                'address_3',
                'city',
                'post_code',
                'country'
            ])->toArray();

            $fields['lat'] = null;
            $fields['long'] = null;
            $fields['user_id'] = $ownerId;

            $result = @json_decode(file_get_contents(sprintf(
                "https://maps.googleapis.com/maps/api/geocode/json?address=%s&key=%s",
                urlencode(implode(",", $data)),
                config('services.google.key')
            )));

            if (isset($result->status) && $result->status = "OK") {
                $fields['lat'] = $result->results[0]->geometry->location->lat;
                $fields['long'] = $result->results[0]->geometry->location->lng;
            }

            return Collection::make($this->customerRepository->update($ownerId, $id, $fields));
        }

        /**
         * @param  string $ownerId
         * @param  array  $fields
         *
         * @return \Illuminate\Support\Collection
         */
        public function create(string $ownerId, array $fields): Collection
        {
            $data = Collection::make($fields);
            $data = $data->only([
                'address_1',
                'address_2',
                'address_3',
                'city',
                'post_code',
                'country'
            ])->toArray();

            $fields['lat'] = null;
            $fields['long'] = null;
            $fields['user_id'] = $ownerId;

            $result = @json_decode(file_get_contents(sprintf(
                "https://maps.googleapis.com/maps/api/geocode/json?address=%s&key=%s",
                urlencode(implode(",", $data)),
                config('services.google.key')
            )));

            if (isset($result->status) && $result->status = "OK") {
                $fields['lat'] = $result->results[0]->geometry->location->lat;
                $fields['long'] = $result->results[0]->geometry->location->lng;
            }

            return Collection::make($this->customerRepository->create($ownerId, $fields));
        }
    }
}
