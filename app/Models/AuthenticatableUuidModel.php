<?php

declare(strict_types=1);

namespace App\Models
{
    use Illuminate\Notifications\Notifiable;
    use Illuminate\Database\Eloquent\Factories\HasFactory;
    use Laravel\Fortify\TwoFactorAuthenticatable;
    use Illuminate\Foundation\Auth\User as Authenticatable;
    use App\Traits\Uuid;

    abstract class AuthenticatableUuidModel extends Authenticatable
    {
        use Notifiable;
        use Uuid;
        use HasFactory;
        use TwoFactorAuthenticatable;

        /**
         * The "type" of the auto-incrementing ID.
         *
         * @var string
         */
        protected $keyType = 'string';

        /**
         * Indicates if the IDs are auto-incrementing.
         *
         * @var bool
         */
        public $incrementing = false;

        /**
         * Primary Keys are UUIDv4 strings instead on integers
         */
        protected static function boot()
        {
            parent::boot();

            static::creating(function ($model) {
                $model->{$model->getKeyName()} = static::generate_uuid();
            });
        }
    }
}
