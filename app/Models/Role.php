<?php

declare(strict_types=1);

namespace App\Models
{
    use Illuminate\Database\Eloquent\Factories\HasFactory;

    class Role extends UuidModel
    {
        use HasFactory;

        protected $fillable = [
            'name'
        ];

        public function users()
        {
            return $this->hasMany(User::class);
        }

        public function permissions()
        {
            return $this->belongsToMany(Permission::class);
        }
    }
}
