<?php

declare(strict_types=1);

namespace App\Models
{
    use Illuminate\Database\Eloquent\Factories\HasFactory;

    class Permission extends UuidModel
    {
        use HasFactory;

        protected $fillable = [
            'name'
        ];

        public function roles()
        {
            return $this->belongsToMany(Role::class);
        }
    }
}
