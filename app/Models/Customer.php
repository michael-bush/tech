<?php

declare(strict_types=1);

namespace App\Models
{
    use Illuminate\Database\Eloquent\Factories\HasFactory;
    use App\Traits\Encryptable;

    class Customer extends UuidModel
    {
        use HasFactory;

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'user_id',
            'company_name',
            'first_name',
            'last_name',
            'phone_number'
        ];

        public function user()
        {
            return $this->belongsTo(User::class);
        }

        public function address()
        {
            return $this->hasOne(Address::class);
        }
    }
}
