<?php

declare(strict_types=1);

namespace App\Models
{
    use Illuminate\Database\Eloquent\Factories\HasFactory;
    use App\Traits\Encryptable;

    class Address extends UuidModel
    {
        use HasFactory;

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'customer_id',
            'address_1',
            'address_2',
            'address_3',
            'city',
            'country',
            'post_code',
            'lat',
            'long'
        ];

        public function customer()
        {
            return $this->belongsTo(Customer::class);
        }
    }
}
