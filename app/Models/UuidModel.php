<?php

declare(strict_types=1);

namespace App\Models
{
    use Illuminate\Database\Eloquent\Factories\HasFactory;
    use Illuminate\Database\Eloquent\Model;
    use App\Traits\Uuid;

    abstract class UuidModel extends Model
    {
        use Uuid;
        use HasFactory;

        /**
         * The "type" of the auto-incrementing ID.
         *
         * @var string
         */
        protected $keyType = 'string';

        /**
         * Indicates if the IDs are auto-incrementing.
         *
         * @var bool
         */
        public $incrementing = false;

        /**
         * Primary Keys are UUIDv4 strings instead on integers
         */
        protected static function boot()
        {
            parent::boot();

            static::creating(function ($model) {
                $model->{$model->getKeyName()} = static::generate_uuid();
            });
        }
    }
}
