<?php

declare(strict_types=1);

namespace App\Models
{
    use Illuminate\Contracts\Auth\MustVerifyEmail;

    class User extends AuthenticatableUuidModel implements MustVerifyEmail
    {
        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'username',
            'email',
            'password',
            'role_id'
        ];

        /**
         * The attributes that should be hidden for arrays.
         *
         * @var array
         */
        protected $hidden = [
            'password',
            'remember_token',
        ];

        /**
         * The attributes that should be cast to native types.
         *
         * @var array
         */
        protected $casts = [
            'email_verified_at' => 'datetime',
        ];

        public function role()
        {
            return $this->belongsTo(Role::class);
        }

        public function customers()
        {
            return $this->hasMany(Customer::class);
        }
    }
}
