<?php

declare(strict_types=1);

namespace App\Traits
{
    trait Uuid
    {
        private static function convertStringBase($numstring, int $frombase = 10, int $tobase = 16)
        {
            $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $tostring = substr($chars, 0, $tobase);
            $numstring = strval($numstring);
            $length = strlen($numstring);
            $result = '';
            $number = [0];

            for ($i = 0; $i < $length; $i++) {
                $number[$i] = strpos($chars, $numstring[$i]);
            }

            do {
                $divide = 0;
                $newlen = 0;

                for ($i = 0; $i < $length; $i++) {
                    $divide = $divide * $frombase + $number[$i];

                    if ($divide >= $tobase) {
                        $number[$newlen++] = (int)($divide / $tobase);
                        $divide = $divide % $tobase;
                    } else if ($newlen > 0) {
                        $number[$newlen++] = 0;
                    }
                }

                $length = $newlen;
                $result = $tostring[$divide] . $result;
            } while ($newlen != 0);

            return $result;
        }

        public static function generate_uuid()
        {
            $version = '0006';
            $offset = 12219292800;

            list($usec, $sec) = explode(" ", microtime());

            $gregorianseconds = $sec + $offset;
            $nano100s = substr($usec, 2, 7);
            $gregorian = $gregorianseconds . $nano100s;
            $bin = static::convertStringBase($gregorian, 10, 2);
            $binpad =  str_pad($bin, 60, '0', STR_PAD_LEFT);

            $clockSeq = (mt_rand( 0, 0x3fff ) | 0x8000);
            $clockSeqHex = str_pad(static::convertStringBase($clockSeq, 10, 16), 4, '0', STR_PAD_LEFT);

            $timeLow = (substr($binpad, -32));
            $timeLowHex = str_pad(static::convertStringBase($timeLow, 2, 16), 8, '0', STR_PAD_LEFT);

            $timeMid = (substr($binpad, -48, 16));
            $timeMidHex = str_pad(static::convertStringBase($timeMid, 2, 16), 4, '0', STR_PAD_LEFT);

            $timeHi = (substr($binpad, 0, 12));
            $timeHiAndVersion = $version . $timeHi;
            $timeHiAndVersionHex = str_pad(static::convertStringBase($timeHiAndVersion, 2, 16), 4, '0', STR_PAD_LEFT);
            $shard = substr(hash_hmac('sha256', config('app.shard'), config('app.key')), 0, 12);

            $uuid  = $shard . $timeHiAndVersionHex . $timeMidHex . $timeLowHex . $clockSeqHex;

            $components = [
                substr($uuid, 0, 8),
                substr($uuid, 8, 4),
                substr($uuid, 12, 4),
                substr($uuid, 16, 4),
                substr($uuid, 20, 12),
            ];

            $uuid = strtolower(implode('-', $components));

            return $uuid;
        }
    }
}
