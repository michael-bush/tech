<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class SafePassword implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed   $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $input = sha1($value);
        $contents = file_get_contents('https://api.pwnedpasswords.com/range/' . substr($input, 0, 5));
        $passwords = explode("\n", $contents);

        foreach($passwords as $password) {
            $password = trim($password);
            $password = explode(":", $password);
            $password = $password[0];

            if (hash_equals(strtolower($password), substr($input, 5))) {
                return false;
            }
        }
 
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'A data breach on site or app has exposed this password making it unsafe to use.';
    }
}
