<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Home@welcome')->name('welcome');
Route::post('/send-recovery-code', 'Auth@sendRecoveryCode')->name('auth.send-recovery-code');

Route::middleware(['auth'])->group(function () {
    Route::get('/email/verify', 'Auth@verify')->name('verification.notice');
    Route::post('/email/verification-notification', 'Auth@resend')->middleware(['throttle:6,1'])->name('verification.resend');

    Route::middleware(['verified'])->group(function () {
        Route::get('/home', 'Home@index')->name('home');
        
        Route::get('/customers/{id}/show', 'Customer@show')->name('customers.show');
        Route::post('/customers/{id}/delete', 'Customer@destroy')->name('customers.delete');
        Route::get('/customers/{id}/edit', 'Customer@edit')->name('customers.edit');
        Route::post('/customers/{id}/edit', 'Customer@update');
        Route::get('/customers/create', 'Customer@create')->name('customers.create');
        Route::post('/customers/create', 'Customer@store');
        Route::get('/customers/table', 'Customer@api')->name('customers.api');
        Route::get('/customers', 'Customer@index')->name('customers');
    });
});
