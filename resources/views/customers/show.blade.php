@extends('layouts.app')

@section('content')
<div class="container">
    @if (session('status'))
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="mt-4 alert alert-info alert-dismissible fade show" role="alert">
                <h4 class="alert-heading">Attention!</h4>
                <p>{{ session('status') }}</p>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>
    @endif
    <div class="row justify-content-center mt-4">
        <div class="col-md-10">
            <div class="mt-sm-5">
                <h4 class="pb-4 border-bottom">Edit Customer</h4>
                <div class="row">
                    <div class="col-12 text-right">
                        <form method="post" action="{{ route('customers.delete', $customer['id']) }}">
                            @csrf
                            <a href="{{ route('customers.edit', $customer['id']) }}" class="btn btn-xs btn-success">Edit</a> <input type="submit" class="btn btn-xs btn-danger" value="Delete">
                        </form>
                    </div>
                </div>
                <div class="row"><div class="col-12"><hr /></div></div>
                <div class="row">
                <div id="map" class="col-12" data-lat="{{ $customer['address']['lat'] }}" data-long="{{ $customer['address']['long'] }}"></div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="py-2">
                            <div class="row py-2">
                                <div class="col-md-6"> <label>First Name</label> <input type="text" class="form-control-plaintext" value="{{ $customer['first_name'] }}" /> </div>
                                <div class="col-md-6 pt-md-0 pt-3"> <label>Last Name</label> <input type="text" class="form-control-plaintext" value="{{ $customer['last_name'] }}" /> </div>
                            </div>
                            <div class="row py-2">
                                <div class="col-md-6"> <label>Organization</label> <input type="text" class="form-control-plaintext" value="{{ $customer['company_name'] }}" /> </div>
                                <div class="col-md-6 pt-md-0 pt-3"> <label>Phone Number</label> <input type="tel" class="form-control-plaintext" required value="{{ $customer['phone_number'] }}" /> </div>
                            </div>
                            <div class="row py-2">
                                <div class="col-md-6"> <label>Address Line 1</label> <input type="text" class="form-control-plaintext" value="{{ $customer['address']['address_1'] }}" /> </div>
                                <div class="col-md-6 pt-md-0 pt-3"> <label>Address Line 2</label> <input type="text" class="form-control-plaintext" value="{{ $customer['address']['address_2'] }}" /> </div>
                            </div>
                            <div class="row py-2">
                                <div class="col-md-6"> <label>Address Line 3</label> <input type="text" class="form-control-plaintext" value="{{ $customer['address']['address_3'] }}" /> </div>
                                <div class="col-md-6 pt-md-0 pt-3"> <label>City</label> <input type="text" class="form-control-plaintext" value="{{ $customer['address']['city'] }}" /> </div>
                            </div>
                            <div class="row py-2">
                                <div class="col-md-6">
                                    <label>Country</label>
                                    <input type="text" class="form-control-plaintext" value="{{ $customer['address']['country'] }}" />
                                </div>
                                <div class="col-md-6 pt-md-0 pt-3">
                                    <label>Postal/Zip Code</label>
                                    <input type="text" class="form-control-plaintext" value="{{ $customer['address']['post_code'] }}" />
                                </div>
                            </div>
                            <div class="py-3 pb-4">
                                <a href="{{ route('customers') }}" class="btn btn-light">Back</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key={{ config('services.google.key') }}&callback=initMap" defer></script>
@endsection