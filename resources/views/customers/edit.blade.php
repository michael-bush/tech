@extends('layouts.app')

@section('content')
<div class="container">
    @if (session('status'))
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="mt-4 alert alert-info alert-dismissible fade show" role="alert">
                <h4 class="alert-heading">Attention!</h4>
                <p>{{ session('status') }}</p>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>
    @endif
    <div class="row justify-content-center mt-4">
        <div class="col-md-10">
            <div class="mt-sm-5">
                <h4 class="pb-4 border-bottom">Edit Customer</h4>
                <div class="py-2">
                    <form method="post" action="{{ route('customers.edit', $customer['id']) }}">
                        @csrf
                        <div class="row py-2">
                            <div class="col-md-6"> <label for="first_name">First Name</label> <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" autocomplete="given-name" value="{{ old('first_name', $customer['first_name']) }}" required /> </div>
                            <div class="col-md-6 pt-md-0 pt-3"> <label for="last_name">Last Name</label> <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" autocomplete="family-name" value="{{ old('last_name', $customer['last_name']) }}" required /> </div>
                        </div>
                        <div class="row py-2">
                            <div class="col-md-6"> <label for="company_name">Organization</label> <input id="company_name" type="text" class="form-control @error('company_name') is-invalid @enderror" name="company_name" autocomplete="organization" value="{{ old('company_name', $customer['company_name']) }}" /> </div>
                            <div class="col-md-6 pt-md-0 pt-3"> <label for="phone">Phone Number</label> <input id="phone" type="tel" class="form-control @error('phone_number') is-invalid @enderror" name="phone_number" autocomplete="tel" value="{{ old('phone_number', $customer['phone_number']) }}" required /> </div>
                        </div>
                        <div class="row py-2">
                            <div class="col-md-6"> <label for="address_1">Address Line 1</label> <input id="address_1" type="text" class="form-control @error('address_1') is-invalid @enderror" name="address_1" autocomplete="address-line1" value="{{ old('address_1', $customer['address']['address_1']) }}" required /> </div>
                            <div class="col-md-6 pt-md-0 pt-3"> <label for="address_2">Address Line 2</label> <input id="address_2" type="text" class="form-control @error('address_2') is-invalid @enderror" name="address_2" autocomplete="address-line2" value="{{ old('address_2', $customer['address']['address_2']) }}" /> </div>
                        </div>
                        <div class="row py-2">
                            <div class="col-md-6"> <label for="address_3">Address Line 3</label> <input id="address_3" type="text" class="form-control @error('address_3') is-invalid @enderror" name="address_3" autocomplete="address-line3" value="{{ old('address_3', $customer['address']['address_3']) }}" /> </div>
                            <div class="col-md-6 pt-md-0 pt-3"> <label for="city">City</label> <input id="city" type="text" class="form-control @error('city') is-invalid @enderror" name="city" autocomplete="address-level2" value="{{ old('city', $customer['address']['city']) }}" required /> </div>
                        </div>
                        <div class="row py-2">
                            <div class="col-md-6">
                                <label for="country">Country</label>
                                <input id="country" type="text" class="form-control @error('country') is-invalid @enderror" name="country" autocomplete="country-name" value="{{ old('country', $customer['address']['country']) }}" required />
                            </div>
                            <div class="col-md-6 pt-md-0 pt-3">
                                <label for="post_code">Postal/Zip Code</label>
                                <input id="post_code" type="text" class="form-control @error('post_code') is-invalid @enderror" name="post_code" autocomplete="postal-code" value="{{ old('post_code', $customer['address']['post_code']) }}" required />
                            </div>
                        </div>
                        <div class="py-3 pb-4">
                            <button class="btn btn-primary mr-3">Save Changes</button>
                            <a href="{{ route('customers.show', $customer['id']) }}" class="btn btn-light">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
