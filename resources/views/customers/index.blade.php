@extends('layouts.app')

@section('content')
<div class="container">
    @if (session('status'))
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="mt-4 alert alert-info alert-dismissible fade show" role="alert">
                <h4 class="alert-heading">Attention!</h4>
                <p>{{ session('status') }}</p>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>
    @endif
    <div class="row justify-content-center mt-4">
        <div class="col-md-10">
            <div class="mt-sm-5">
                <h4 class="mb-4 border-bottom">Customers</h4>
                <div class="row">
                    <div class="col-12 text-right">
                        <a href="{{ route('customers.create') }}" class="btn btn-xs btn-success">Create</a>
                    </div>
                </div>
                <div class="row"><div class="col-12"><hr /></div></div>
                <div class="row">
                    <div class="col-12">
                        <table class="table table-bordered" id="customers-table">
                            <thead>
                                <tr>
                                    <th>Company Name</th>
                                    <th>First name</th>
                                    <th>Last name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
       </div>
    </div>
</div>
@endsection
