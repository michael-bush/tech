@extends('layouts.app')

@section('content')
<div class="container">
    @if (session('status'))
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="mt-4 alert alert-info alert-dismissible fade show" role="alert">
                <h4 class="alert-heading">Attention!</h4>
                <p>{{ __('auth.' . session('status')) }}</p>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>
    @endif
    <div class="row justify-content-center mt-4">
        <div class="col-md-10">
            <div class="mt-sm-5">
                <h4 class="pb-4 border-bottom">Account Settings</h4>
                <div class="py-3">
                    <div class="col-md-6 p-0">
                        <h5>Account Email<small class="text-danger font-weight-bold"><br />Changing your email address will lock your account until the new email has been verified!</small></h5>
                    </div>
                    <form method="post" action="{{ route('user-profile-information.update') }}">
                        @csrf
                        @method('PUT')
                        <div class="row py-2">
                            <div class="col-md-6">
                                <label for="email">Email</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" required autocomplete="email" value="{{ old('email', Auth::user()->email) }}" />
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="py-3 pb-4 border-bottom">
                            <button type="submit" class="btn btn-primary mr-3">Save Changes</button>
                            <button type="button" class="btn btn-light">Cancel</button>
                        </div>
                    </form>
                </div>
                <div class="py-2">
                    <h5>Account Password</h5>
                    <form method="post" action="{{ route('user-password.update') }}">
                        @csrf
                        @method('PUT')
                        <div class="row py-2">
                            <div class="col-md-6">
                                <label for="password">New Password</label>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" />
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="col-md-6 pt-md-0 pt-3">
                                <label for="password-confirm">Confirm New Password</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" />
                            </div>
                        </div>
                        <div class="py-3 pb-4 border-bottom">
                            <button class="btn btn-primary mr-3">Save Changes</button>
                            <button class="btn btn-light">Cancel</button>
                        </div>
                   </form>
               </div>
              <div class="row py-2">
                    <div class="col-md-6">
                        <h5>2-Step Verification<small class="text-danger font-weight-bold"><br />Increases your account security with 2-Step Verification, using this feature requires a 3rd-party application to be installed on your device!</small></h5>
                        <form method="post" action="/user/two-factor-authentication">
                          @csrf
                          <div class="py-3 pb-4 border-bottom">
                              @if (auth()->user()->two_factor_secret)
                              <div class="row mb-4">
                                  <div class="col-12">
                                      <h5 class="font-weight-bold">Step 1<br /><small>Get the Google Authenticator application for your device now</small></h5>
                                  </div>
                                  <div class="d-flex align-items-center">
                                      <div class="col-6">
                                          <a href="https://apps.apple.com/gb/app/google-authenticator/id388497605" target="_blank">
                                              <img src="/images/app-store.png" height="48" alt="App Store" />
                                          </a>
                                      </div>
                                      <div class="col-6">
                                          <a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2" target="_blank">
                                              <img src="/images/play-store.png" height="48" alt="Play Store" />
                                          </a>
                                      </div>
                                  </div>
                              </div>
                              @method('DELETE')
                              <div class="pb-5">
                                  <h5 class="font-weight-bold">Step 2<br /><small>Scan this QR code with the Google Authenticator application</small></h5>
                                  {!! auth()->user()->twoFactorQrCodeSvg() !!}
                              </div>
                              <button type="submit" class="btn btn-danger">Disable 2-Step Verification</button>
                              @else
                              <div class="row mb-4">
                                  <div class="col-12">
                                      <h6 class="font-weight-bold">Get the Google Authenticator application for your device now before you enable 2-Step Verification!</h6>
                                  </div>
                                  <div class="d-flex align-items-center">
                                      <div class="col-6">
                                          <a href="https://apps.apple.com/gb/app/google-authenticator/id388497605" target="_blank">
                                              <img src="/images/app-store.png" height="48" alt="App Store" />
                                          </a>
                                      </div>
                                      <div class="col-6">
                                          <a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2" target="_blank">
                                              <img src="/images/play-store.png" height="48" alt="Play Store" />
                                          </a>
                                      </div>
                                  </div>
                              </div>
                              <button type="submit" class="btn btn-primary">Enable 2-Step Verification</button>
                              @endif
                          </div>
                      </form>
                  </div>
              </div>
              <div class="d-sm-flex align-items-center pt-3" id="deactivate">
                 <div>
                    <h5>Deactivate your account<br /><small class="text-danger font-weight-bold">Removes all information about yourself from the platform after 28 days!</small></h5>
                 </div>
                 <div class="ml-auto">
                     <button class="btn btn-outline-danger">Deactivate</button>
                 </div>
             </div>
           </div>
       </div>
    </div>
</div>
@endsection
