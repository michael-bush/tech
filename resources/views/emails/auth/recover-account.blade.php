@component('mail::message')
# Account Recovery

We are sorry to hear that you have not been able to sign in using 2-Step Verification.

Below you find a single use recover code you should use the next time you sign in.

## Recovery Code

@component('mail::panel')
{{ $recoveryCode }}
@endcomponent

@component('mail::button', ['url' => route('login')])
Proceed back to login
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
