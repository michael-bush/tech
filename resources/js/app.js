require('./bootstrap');

window.addEventListener('DOMContentLoaded', () => {
    $('#customers-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/customers/table',
        columns: [
            {data: 'company_name', name: 'company_name'},
            {data: 'first_name', name: 'first_name'},
            {data: 'last_name', name: 'last_name'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
});

window.initMap = function() {
    let element = document.getElementById('map');
    let lat = element.dataset.lat;
    let long = element.dataset.long;

    if (lat && long) {
        const LatLng = {lat: parseFloat(lat), lng: parseFloat(long)};

        let map = new google.maps.Map(document.getElementById('map'), {
            center: LatLng,
            zoom: 20
        });

        new google.maps.Marker({
            position: LatLng,
            map
        });
    }
};
