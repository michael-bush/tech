<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Route;
use App\Models\Permission;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ignore = [
            'login',
            'logout',
            'password.request',
            'password.reset',
            'password.email',
            'password.update',
            'register',
            'verification.notice',
            'verification.verify',
            'verification.resend',
            'verification.send',
            'user-profile-information.update',
            'user-password.update',
            'password.confirm',
            'password.confirmation',
            'two-factor.login',
            'livewire.upload-file',
            'livewire.preview-file',
            'home',
            'auth.send-recovery-code',
            'welcome'
        ];

        $routes = Route::getRoutes();

        foreach ($routes as $route) {
            $name = $route->getName();

            if (empty($name)) {
                continue;
            }

            if (in_array($name, $ignore)) {
                continue;
            }

            if (Permission::where(['name' => 'admin'])->count() > 0) {
                continue;
            }

            Permission::create(['name' => $name]);
        }
    }
}
